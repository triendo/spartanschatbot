#TUTORIAL: https://dialogflow-python-client-v2.readthedocs.io/en/latest/
import dialogflow_v2 as dialogflow
import pdb
def detect_intent(project_id, session_id, text, language_code):
    """Returns the result of detected intent with text as input.
    Using the same `session_id` between requests allows continuation
    of the conversation."""

    session_client = dialogflow.SessionsClient()

    session = session_client.session_path(project_id, session_id)
    print('\nSession path: {}'.format(session))

    text_input = dialogflow.types.TextInput(
        text=text, language_code=language_code)

    query_input = dialogflow.types.QueryInput(text=text_input)

    response = session_client.detect_intent(
        session=session, query_input=query_input)

    print('=' * 20)
    print('Query text: {}'.format(response.query_result.query_text))
    print('Detected intent: {0} (confidence: {1} and the payload {2})\n'.format(response.query_result.intent.display_name,response.query_result.intent_detection_confidence,response.query_result.parameters))
    rs = {}
    rs["intent"] = response.query_result.intent.display_name
    rs["payload"] = response.query_result.parameters
    return rs

if __name__ == '__main__':
    detect_intent("spartanschatbot","1","members",'EN')