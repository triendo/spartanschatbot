import time
import slack_utility
import dialogflow_utility
import spartans_utility
import pdb

def handle_command(slack_api, command, channel):
	result = dialogflow_utility.detect_intent("spartanschatbot", "100", command, 'EN')
	responses = spartans_utility.process_intent(result)
	print responses
	for response in responses:
		slack_api.rtm_send_message(channel, str(response))

def main():
	READ_WEBSOCKET_DELAY = 1 # 1 second delay between reading from firehose
	slack_api = slack_utility.connect()
	if slack_api.rtm_connect():
		print 'SPARTANS CHATBOT CONNECTED AND RUNNING'
		while True:
			command, channel = slack_utility.parse_slack_response(slack_api.rtm_read())
			if command and channel:
				handle_command(slack_api, command, channel)
			time.sleep(READ_WEBSOCKET_DELAY)
	else:
		print 'Connection failed. Invalid Slack token or bot ID?' 

if __name__ == '__main__':
	main()